package com.yoki.advent_of_code.utils.data_structure;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Stream;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

class SuffixArrayTest {

  static record UTest<T, U>(T expected, U... input) {}

  @TestFactory
  Stream<DynamicTest> lcs() {
    List<UTest<SortedSet<String>, String>> data = List.of(
        new UTest<>(new TreeSet<>(Set.of("ba")), "atatba", "bbbab"),
        new UTest<>(new TreeSet<>(Set.of("m")), "tdltdtmhl", "RNCBcwmHr"),
        new UTest<>(new TreeSet<>(Set.of("m")), "tdltdtmhl", "RNCBcwmwHr"),
        new UTest<>(new TreeSet<>(Set.of("z")), "WDzDPnvvGnsWLWpGJJ", "HRzCCRZNBRrRwMNwHH"),
        new UTest<>(new TreeSet<>(Set.of("L")), "zLHQSBWmL", "cjdPjLLLZ"),
        new UTest<>(new TreeSet<>(Set.of("z")), "jVdpSLPVSjtNdjdPtptzLNPP", "zcBzwCQrCcTBTRqwwqMCJTcC"),
        new UTest<>(new TreeSet<>(Set.of("Z")),"lhlnWGGFWZhDfgFfWDfrhgr", "RwcccqcZQBQMwcqJMTMRMqJ")
    );

    return data.stream().map(test -> dynamicTest(
        Arrays.toString(test.input) + " -> " + test.expected,
        () -> assertEquals(test.expected, SuffixArray.lcs(test.input))
    ));
  }

}
