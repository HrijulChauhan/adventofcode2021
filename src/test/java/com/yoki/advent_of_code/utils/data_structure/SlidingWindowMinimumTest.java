package com.yoki.advent_of_code.utils.data_structure;

import java.util.List;
import org.junit.Test;

public class SlidingWindowMinimumTest {

  static record UTest(Object expected, Object... input) {}

  @Test
  public void newSliding() {
    SlidingWindowMinimum win = new SlidingWindowMinimum(new int[]{3, 4, 2, 5, 1}, 3);
    for (int i = 0; i < 2; i++) {
      System.out.println(win);
      win.slide();
    }
    System.out.println(win);

    int[] values = {0, 1, 0, 0, 2, 1, -1};
    List<Integer> integers = SlidingWindowMinimum.allMins(values, 1);
    System.out.println(integers);
    System.out.println(values.length);
  }

}
