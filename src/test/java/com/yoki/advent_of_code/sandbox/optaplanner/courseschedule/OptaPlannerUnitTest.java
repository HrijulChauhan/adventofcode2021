package com.yoki.advent_of_code.sandbox.optaplanner.courseschedule;

import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.constraint.CourseScheduleConstraintProvider;
import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain.CourseSchedule;
import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain.Lecture;
import java.time.Duration;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.optaplanner.core.api.score.ScoreManager;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.SolverConfig;

public class OptaPlannerUnitTest {

  private CourseSchedule unsolvedCourseSchedule;

  @Before
  public void setUp() {
    unsolvedCourseSchedule = new CourseSchedule();
    for (long i = 0; i < 7; i++) unsolvedCourseSchedule.getLectures().add(new Lecture(i));
    unsolvedCourseSchedule.getPeriods().addAll(List.of(1, 2, 3));
    unsolvedCourseSchedule.getRooms().addAll(List.of(1, 2));
  }

  @Test
  public void test_whenConstraintStreamSolver() {
    SolverFactory<CourseSchedule> solverFactory = SolverFactory.create(new SolverConfig()
        .withSolutionClass(CourseSchedule.class)
        .withEntityClasses(Lecture.class)
        .withConstraintProviderClass(CourseScheduleConstraintProvider.class)
        .withTerminationSpentLimit(Duration.ofSeconds(1)));

    CourseSchedule solvedCourseSchedule = solverFactory.buildSolver().solve(unsolvedCourseSchedule);

    System.out.println(ScoreManager.create(solverFactory).explainScore(solvedCourseSchedule).getSummary());

    solvedCourseSchedule.printCourseSchedule();
    Assert.assertNotNull(solvedCourseSchedule.getScore());
    Assert.assertEquals(-1, solvedCourseSchedule.getScore().getHardScore());
  }
}
