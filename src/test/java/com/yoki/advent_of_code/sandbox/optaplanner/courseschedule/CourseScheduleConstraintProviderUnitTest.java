package com.yoki.advent_of_code.sandbox.optaplanner.courseschedule;

import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.constraint.CourseScheduleConstraintProvider;
import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain.CourseSchedule;
import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain.Lecture;
import org.junit.Test;
import org.optaplanner.test.api.score.stream.ConstraintVerifier;

public class CourseScheduleConstraintProviderUnitTest {

  private final ConstraintVerifier<CourseScheduleConstraintProvider, CourseSchedule> constraintVerifier =
      ConstraintVerifier.build(new CourseScheduleConstraintProvider(), CourseSchedule.class, Lecture.class);

  @Test
  public void testCourseScheduleConstraint() {
    Lecture lecture = new Lecture(0L);
    lecture.setPeriod(1);
    lecture.setRoomNumber(1);

    Lecture conflictedLecture = new Lecture(1L);
    conflictedLecture.setPeriod(1);
    conflictedLecture.setRoomNumber(1);

    constraintVerifier.verifyThat(CourseScheduleConstraintProvider::roomConflict)
        .given(lecture, conflictedLecture)
        .penalizesBy(1);
  }

}
