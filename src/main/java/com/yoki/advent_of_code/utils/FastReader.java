package com.yoki.advent_of_code.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FastReader implements Closeable {
  BufferedReader br;
  StringTokenizer st;

  public FastReader() {
    br = new BufferedReader(new InputStreamReader(System.in));
  }

  public FastReader(String path) throws FileNotFoundException {
    br = new BufferedReader(new FileReader(path));
  }

  public String next() {
    while (st == null || !st.hasMoreElements()) {
      try {
        st = new StringTokenizer(br.readLine());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return st.nextToken();
  }

  public int nextInt() {return Integer.parseInt(next());}

  public long nextLong() {return Long.parseLong(next());}

  public double nextDouble() {
    return Double.parseDouble(next());
  }

  public String nextLine() {
    String str = "";
    try {
      if (st.hasMoreTokens()) {
        str = st.nextToken("\n");
      } else {
        str = br.readLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return str;
  }

  @Override
  public void close() throws IOException {
    this.br.close();
  }
}
