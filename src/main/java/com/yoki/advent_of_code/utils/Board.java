package com.yoki.advent_of_code.utils;

import java.util.Collection;
import java.util.List;
import lombok.Data;
import one.util.streamex.StreamEx;

@Data
public class Board {

  private List<List<Case>> rows;
  private boolean success;

  public Board(List<List<Case>> rows) {
    this.rows = rows;
  }

  public void check(int nb) {
    StreamEx.of(rows).flatMap(Collection::stream).filterBy(Case::getN, nb)
        .forEach(c -> c.setC(true));
  }

  public boolean getSuccess() {
    for (var line : rows)
      if (line.stream().allMatch(Case::isC)) return true;

    for (int i = 0; i < rows.get(0).size(); i++) {
      int finalI = i;
      if (rows.stream().map(l -> l.get(finalI)).allMatch(Case::isC)) return true;
    }
    return false;
  }

  public boolean isSuccess() {
    if (!getSuccess()) return false;
    success = true;
    return true;
  }

  public int getScore(int success) {
    var sum = StreamEx.of(rows).flatMap(Collection::stream)
        .filter(i -> !i.isC()).mapToInt(Case::getN).sum();
    System.out.println("flat: " + sum);
    System.out.println("success: " + success);
    return sum * success;
  }

  @Data
  public static class Case {
    private int n;
    private boolean c = false;

    public Case(int n) {
      this.n = n;
    }
  }
}

