package com.yoki.advent_of_code.utils;

public interface SStack<T> {
  int depth();
  T top();
  SStack<T> pop();

  default SStack<T> push(T newTop) {
    return new NonEmpty<>(newTop, this);
  }

  class NonEmpty<T> implements SStack<T> {
    private final T top;
    private final SStack<T> tail;

    public NonEmpty(T newTop, SStack<T> previous) {
      top = newTop;
      tail = previous;
    }

    @Override
    public int depth() {
      return 1 + tail.depth();
    }

    @Override
    public T top() {
      return top;
    }

    @Override
    public SStack<T> pop() {
      return tail;
    }
  }

  class Empty<T> implements SStack<T> {
    @Override
    public int depth() {
      return 0;
    }

    @Override
    public T top() {
      throw new IllegalStateException();
    }

    @Override
    public SStack<T> pop() {
      throw new IllegalStateException();
    }
  }
}
