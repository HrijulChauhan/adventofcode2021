package com.yoki.advent_of_code.utils.data_structure;


import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;

import java.util.HashMap;
import java.util.List;
import lombok.NoArgsConstructor;
import one.util.streamex.LongStreamEx;
import one.util.streamex.StreamEx;

@NoArgsConstructor
public class Counter<K> extends HashMap<K, Long> {

  public Counter(List<K> list) {
    super(StreamEx.of(list).groupingBy(identity(), counting()));
  }

  public void decrement(K key) {
    decrement(key, 1);
  }

  public void decrement(K key, long val) {
    computeIfPresent(key, (k, v) -> v - val);
  }

  public void increment(K key) {
    increment(key, 1);
  }

  public void increment(K key, long val) {
    putIfAbsent(key, 0L);
    compute(key, (k,v) -> v + val);
  }

  public long min() {
    return LongStreamEx.of(values()).min().orElse(0);
  }

  public long max() {
    return LongStreamEx.of(values()).max().orElse(0);
  }

  public long sum() {
    return LongStreamEx.of(values()).sum();
  }
}
