package com.yoki.advent_of_code.utils.tailrecursion;

import java.util.stream.Stream;
import org.apache.commons.lang3.NotImplementedException;

/**
Using lazy evaluation to create a java version of Tail Recursion
**/

@FunctionalInterface
public interface TailCall<T> {
  TailCall<T> apply();

  default boolean isComplete() {
    return false;
  }

  default T result() {
    throw new NotImplementedException("Not Implemented");
  }

  default T invoke() {
    return Stream.iterate(this, TailCall::apply)
        .filter(TailCall::isComplete)
        .findFirst().get().result();
  }

  static <T> TailCall<T> done(final T value) {
    return new TailCall<T>() {
      @Override
      public boolean isComplete() {
        return true;
      }

      @Override
      public T result() {
        return value;
      }

      @Override
      public TailCall<T> apply() {
        throw new NotImplementedException("not implemented");
      }
    };
  }
}
