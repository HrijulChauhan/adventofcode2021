package com.yoki.advent_of_code.utils.tailrecursion;

import static com.yoki.advent_of_code.utils.tailrecursion.TailCall.done;

import java.math.BigInteger;

public class Factorial {

  public static void main(String[] args) {
    System.out.println(factorialTailRec(50000, BigInteger.ONE).invoke());
  }

  public static TailCall<BigInteger> factorialTailRec(int n, BigInteger fact) {
    if (n < 2) return done(fact);
    return () -> factorialTailRec(n - 1, fact.multiply(BigInteger.valueOf(n)));
  }

  public static Long factorial(int n) {
    if (n < 2) return 1L;
    return n * factorial(n - 1);
  }

  public static Long factorial(int n, Long fact) {
    if (n < 2) return fact;
    return factorial(n - 1, n * fact);
  }
}
