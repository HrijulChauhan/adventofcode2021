package com.yoki.advent_of_code.utils.algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

  public record Vertex(String label) {}

  private Map<Vertex, List<Vertex>> adjVertices = new HashMap<>();

  public void add(String label1, String label2) {
    Vertex v1 = addVertex(label1);
    Vertex v2 = addVertex(label2);
    adjVertices.get(v1).add(v2);
    adjVertices.get(v2).add(v1);
  }

  public Vertex addVertex(String label) {
    Vertex v = new Vertex(label);
    adjVertices.putIfAbsent(v, new ArrayList<>());
    return v;
  }

  public List<Vertex> getAdjVertices(String label) {
    return adjVertices.get(new Vertex(label));
  }
}
