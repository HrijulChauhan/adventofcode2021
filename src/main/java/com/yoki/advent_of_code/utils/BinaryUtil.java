package com.yoki.advent_of_code.utils;

public class BinaryUtil {
  private BinaryUtil(){}

  public static long bin(String b) {
    return Long.parseLong(b, 2);
  }

  public static byte[] intToLittleEndian(long numero) {
    byte[] b = new byte[4];
    b[0] = (byte) (numero & 0xFF);
    b[1] = (byte) ((numero >> 8) & 0xFF);
    b[2] = (byte) ((numero >> 16) & 0xFF);
    b[3] = (byte) ((numero >> 24) & 0xFF);
    return b;
  }

  public static String hexToBin(String hex) {
    hex = hex.replace("0", "0000");
    hex = hex.replace("1", "0001");
    hex = hex.replace("2", "0010");
    hex = hex.replace("3", "0011");
    hex = hex.replace("4", "0100");
    hex = hex.replace("5", "0101");
    hex = hex.replace("6", "0110");
    hex = hex.replace("7", "0111");
    hex = hex.replace("8", "1000");
    hex = hex.replace("9", "1001");
    hex = hex.replace("A", "1010");
    hex = hex.replace("B", "1011");
    hex = hex.replace("C", "1100");
    hex = hex.replace("D", "1101");
    hex = hex.replace("E", "1110");
    hex = hex.replace("F", "1111");
    return hex;
  }
}
