package com.yoki.advent_of_code.utils.vector;

import com.yoki.advent_of_code.utils.CollectionUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.builder.CompareToBuilder;

@Getter
public class Tuple<T> implements Comparable<Tuple<T>>, Serializable {

  protected final List<T> elems;

  @SafeVarargs
  public Tuple(T... elems) {
    this.elems = CollectionUtil.asList(elems);
  }

  public Tuple(List<T> elems) {
    this.elems = CollectionUtil.asList(elems);
  }

  public T get(int i) {
    return elems.get(i);
  }

  public void set(int i, T elem) {
    elems.set(i, elem);
  }

  public List<T> zip() {
    return CollectionUtil.asList(this.elems);
  }

  @Override
  public int compareTo(Tuple<T> other) {
    return (new CompareToBuilder()).append(this.elems, other.elems).toComparison();
  }

  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj instanceof Tuple other) {
      return this.elems.equals(other.elems);
    }

    return false;
  }

  public int hashCode() {
    return Objects.hashCode(elems);
  }

  public String toString() {
    return "(" + StreamEx.of(elems).joining(",") + ")";
  }
}
