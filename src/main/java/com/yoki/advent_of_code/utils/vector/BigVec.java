package com.yoki.advent_of_code.utils.vector;

import java.math.BigInteger;
import java.util.List;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class BigVec extends Tuple<BigInteger> {

  public BigVec(BigInteger... toList) {
    super(toList);
  }

  public BigVec(List<BigInteger> toList) {
    super(toList);
  }

  public BigVec sub(BigVec other) {
    return new BigVec(
        StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft().add(p.getRight().subtract(p.getRight()))).toList());
  }

  public BigVec add(BigVec other) {
    return new BigVec(StreamEx.zip(this.zip(), other.zip(), Pair::of).map(p -> p.getLeft().add(p.getRight())).toList());
  }

  public BigVec mul(BigInteger n) {
    return new BigVec(StreamEx.of(this.zip()).map(v -> v.multiply(n)).toList());
  }

  public BigVec mul(int n) {
    return mul(new BigInteger(String.valueOf(n)));
  }
}
