package com.yoki.advent_of_code.aoc.days2022;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import com.yoki.advent_of_code.utils.data_structure.SuffixArray;
import java.io.PrintStream;
import java.util.List;
import one.util.streamex.StreamEx;

public class Day3 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day3(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    var res = this.input.lines()
        .map(StringUtil::splitInHalf)
        .map(this::getCommonChar)
        .mapToInt(this::computePriority)
        .sum();
    return String.valueOf(res);
  }


  public String part2() {
    var res = StreamEx.ofSubLists(this.input.lines().toList(), 3)
        .map(this::getCommonChar)
        .mapToInt(this::computePriority)
        .sum();
    return String.valueOf(res);
  }

  private int computePriority(String i) {
    return StringUtil.ALPHA.indexOf(i) + 1;
  }

  public String getCommonChar(List<String> inputs) {
    String first = SuffixArray.lcs(inputs.toArray(new String[0])).first();
    return first.length() > 1 ? first.split("")[0] : first;
  }

}
