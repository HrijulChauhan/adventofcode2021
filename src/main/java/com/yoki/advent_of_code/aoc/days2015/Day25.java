package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;

public class Day25 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day25(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(0);
  }

  public String part2() {
    return String.valueOf(0);
  }
}