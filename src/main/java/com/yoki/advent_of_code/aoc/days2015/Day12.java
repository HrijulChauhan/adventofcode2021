package com.yoki.advent_of_code.aoc.days2015;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.github.fge.jackson.JsonLoader;
import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

public class Day12 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day12(String input, PrintStream output) {
    super(input, output);
  }

  @SneakyThrows
  public String part1() {
    var json = JsonLoader.fromString(this.input);
    return String.valueOf(sumAllNumbers(json));
  }

  @SneakyThrows
  public String part2() {
    var json = JsonLoader.fromString(this.input);
    return String.valueOf(sumAllNumbersIgnoring(json, "red"));
  }

  private int sumAllNumbers(JsonNode node) {
    return sumAllNumbersIgnoring(node, "");
  }

  private int sumAllNumbersIgnoring(JsonNode node, String ignoring) {
    int res = 0;
    res += switch (node.getNodeType()) {
      case NUMBER -> node.intValue();
      case OBJECT, ARRAY -> handleContainersIgnoring(node, ignoring);
      case BINARY, BOOLEAN, MISSING, NULL, STRING, POJO -> 0;
    };
    return res;
  }

  private int handleContainersIgnoring(JsonNode node, String ignoring) {
    if (StringUtils.isNotEmpty(ignoring) && !node.isArray())
      for (var n : node)
        if (n.getNodeType() == JsonNodeType.STRING && n.textValue().equals(ignoring))
          return 0;

    int res = 0;
    for (var n : node) res += sumAllNumbersIgnoring(n, ignoring);
    return res;
  }
}