package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day7 extends AocDay {

  private final List<String> ip;
  Map<String, Circuit> circuitMap = new HashMap<>();

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day7(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    for (String line : ip) {
      String[] split = line.split(" -> ");
      circuitMap.put(split[1], new Circuit(split[0]));
    }
    out.println("Part 1: " + circuitMap.get("a").eval());
    return String.valueOf(circuitMap.get("a").eval());
  }

  public String part2() {
    for (String line : ip) {
      String[] split = line.split(" -> ");
      circuitMap.put(split[1], new Circuit(split[0]));
    }
    // Change in the input file:
    //    14146 -> b  to 956 -> b

    return String.valueOf(circuitMap.get("a").eval());
  }

  class Circuit {
    String op;
    String a;
    String b;
    int value = -1;

    public Circuit(String op) {
      if (op.contains("NOT")) {
        this.op = "NOT";
        this.a = op.replace("NOT ", "");
      } else if (!op.contains(" ")) {
        this.op = "ID";
        this.a = op;
      } else {
        String[] split = op.split(" ");
        this.a = split[0];
        this.op = split[1];
        this.b = split[2];
      }
    }

    public int eval() {
      if (this.value != -1) return this.value;
      this.value = switch (this.op) {
        case "AND" -> getVal(a) & getVal(b);
        case "OR" -> getVal(a) | getVal(b);
        case "LSHIFT" -> getVal(a) << getVal(b);
        case "RSHIFT" -> getVal(a) >> getVal(b);
        case "NOT" -> 65535 - getVal(a);
        default -> getVal(a);
      };
      return value;
    }

    public int getVal(String n) {
      try {
        return Integer.parseInt(n);
      } catch (Exception ignored) {
        return circuitMap.get(n).eval();
      }
    }

    @Override
    public String toString() {
      if (this.value != -1) return String.valueOf(value);
      return a + " " + op + " " + b;
    }
  }
}