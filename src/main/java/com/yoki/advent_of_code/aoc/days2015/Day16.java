package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.Integer.parseInt;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;

public class Day16 extends AocDay {


  private final List<Aunt> aunts;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day16(String input, PrintStream output) {
    super(input, output);
    this.aunts = this.input.lines().map(Aunt::fromString).toList();
  }

  public String part1() {
    var info = List.of(
        new Info("children", 3, Type.EXACT),
        new Info("cats", 7, Type.EXACT),
        new Info("samoyeds", 2, Type.EXACT),
        new Info("pomeranians", 3, Type.EXACT),
        new Info("akitas", 0, Type.EXACT),
        new Info("vizslas", 0, Type.EXACT),
        new Info("goldfish", 5, Type.EXACT),
        new Info("trees", 3, Type.EXACT),
        new Info("cars", 2, Type.EXACT),
        new Info("perfumes", 1, Type.EXACT)
    );
    return aunts.stream().filter(a -> a.isGoodAunt(info)).findFirst().map(String::valueOf).orElse("");
  }

  public String part2() {
    var info = List.of(
        new Info("cats", 7, Type.HIGHER),
        new Info("trees", 3, Type.HIGHER),
        new Info("pomeranians", 3, Type.LOWER),
        new Info("goldfish", 5, Type.LOWER),
        new Info("samoyeds", 2, Type.EXACT),
        new Info("children", 3, Type.EXACT),
        new Info("akitas", 0, Type.EXACT),
        new Info("vizslas", 0, Type.EXACT),
        new Info("cars", 2, Type.EXACT),
        new Info("perfumes", 1, Type.EXACT)
    );
    return aunts.stream().filter(a -> a.isGoodAunt(info)).findFirst().map(String::valueOf).orElse("");
  }

  enum Type {
    EXACT, LOWER, HIGHER
  }

  record Info(String name, int value, Type type) {

    public boolean notMatching(Info other) {
      return switch (type) {
        case EXACT -> value != other.value;
        case HIGHER -> value >= other.value;
        case LOWER -> value <= other.value;
      };
    }
  }

  record Aunt(int n, List<Info> info) {

    public boolean isGoodAunt(List<Info> known) {
      for (var e : info) {
        Optional<Info> knowValue = known.stream().filter(i -> i.name.equals(e.name)).findFirst();
        if (knowValue.isEmpty()) return false;
        if (knowValue.get().notMatching(e)) return false;
      }
      return true;
    }

    static Aunt fromString(String line) {
      var s = line.replace(",", "").replace(":", "").split(" ");
      return new Aunt(parseInt(s[1]), List.of(
          new Info(s[2], parseInt(s[3]), Type.EXACT),
          new Info(s[4], parseInt(s[5]), Type.EXACT),
          new Info(s[6], parseInt(s[7]), Type.EXACT)));
    }
  }
}