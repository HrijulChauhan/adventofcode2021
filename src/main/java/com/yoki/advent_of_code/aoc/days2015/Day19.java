package com.yoki.advent_of_code.aoc.days2015;

import static com.yoki.advent_of_code.utils.StringUtil.replaceAt;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import one.util.streamex.StreamEx;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

public class Day19 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day19(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    MoleculeMedicine molecule = new MoleculeMedicine(this.input);
    return String.valueOf(molecule.computeAllMolecule().size());
  }

  public String part2() {
    MoleculeMedicine molMed = new MoleculeMedicine(this.input);
    return String.valueOf(molMed.generateFromOneElectron());
  }

  static class MoleculeMedicine {

    MultiValuedMap<String, String> replacements;
    String molecule;

    public MoleculeMedicine(String input) {
      List<String> strings = input.lines().toList();
      molecule = strings.get(strings.size() - 1).strip();
      replacements = new ArrayListValuedHashMap<>();
      StreamEx.of(strings)
          .filter(l -> l.contains("=>"))
          .map(i -> i.split(" => "))
          .forEach(i -> replacements.put(i[0], i[1]));
    }

    public int generateFromOneElectron() {
      int res = 0;
      while (!molecule.equals("e")) {
        for (var key : replacements.asMap().entrySet()) {
          for (var value : key.getValue()) {
            if (molecule.contains(value)) {
              molecule = replaceAt(molecule, value, key.getKey(), molecule.indexOf(value));
//              System.out.println(molecule);
              res++;
            }
          }
        }
      }
      return res;
    }

    public Set<String> computeAllMolecule() {
      return computeAllMolecule(this.molecule);
    }

    private Set<String> computeAllMolecule(String molecule) {
      Set<String> tried = new HashSet<>();
      for (var replace : replacements.asMap().entrySet()) {
        for (var value : replace.getValue()) {
          int i = 0;
          String afterReplacement;
          while (!(afterReplacement = StringUtil.replaceNthOccurrence(molecule, replace.getKey(), value, i)).equals(molecule)) {
            tried.add(afterReplacement);
            i++;
          }
        }
      }
      return tried;
    }
  }
}