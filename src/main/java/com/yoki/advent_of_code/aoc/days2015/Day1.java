package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.List;

public class Day1 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    List<Character> line = StringUtil.stringToChars(this.input);
    return String.valueOf(CollectionUtil.enumerateStream(line).mapToInt(c -> c.item() == '(' ? 1 : -1).sum() + 1);
  }

  public String part2() {
    List<Character> line = StringUtil.stringToChars(this.input);
    int floor = 0;
    for (var c : CollectionUtil.enumerate(line)) {
      if (floor == -1) {
        return String.valueOf(c.index());
      }
      floor += c.item() == '(' ? 1 : -1;
    }
    return "";
  }
}