package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.Math.abs;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class Day5 extends AocDay {

  private final List<String> ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day5(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    return String.valueOf(ip.stream().filter(this::niceString).count());
  }

  public String part2() {
    return String.valueOf(ip.stream().filter(this::niceStringPart2).count());
  }

  public boolean niceStringPart2(String s) {
    List<Character> chars = StringUtil.stringToChars(s);
    return twoPairs(chars) && sandwichLetter(chars);
  }

  private boolean twoPairs(List<Character> chars) {
    for (int i = 1; i < chars.size(); i++) {
      char[] pair = new char[]{chars.get(i - 1), chars.get(i)};
      for (int j = 1; j < chars.size(); j++) {
        if (Arrays.equals(pair, new char[]{chars.get(j - 1), chars.get(j)}) && abs(i - j) > 1) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean sandwichLetter(List<Character> chars) {
    for (int i = 2; i < chars.size(); i++) {
      if (chars.get(i - 2).equals(chars.get(i)))
        return true;
    }
    return false;
  }

  public boolean niceString(String s) {
    List<String> not = List.of("ab", "cd", "pq", "xy");
    if (not.stream().anyMatch(s::contains)) return false;

    List<Character> chars = StringUtil.stringToChars(s);

    List<Character> vowels = List.of('a', 'e', 'i', 'o', 'u');
    if (chars.stream().filter(vowels::contains).count() < 3) return false;

    for (int i = 1; i < chars.size(); i++) {
      if (chars.get(i - 1).equals(chars.get(i))) return true;
    }
    return false;
  }
}