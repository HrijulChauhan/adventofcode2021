package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day3 extends AocDay {

  private final List<Character> formattedInput;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day3(String input, PrintStream output) {
    super(input, output);
    this.formattedInput = StringUtil.stringToChars(this.input.trim());
  }

  public String part1() {
    Set<Pos> visitedHouses = new HashSet<>();
    Pos cur = new Pos(0,0);
    visitedHouses.add(cur);
    for (char d : this.formattedInput) {
      cur = switch (d) {
        case '>' -> new Pos(cur.x+1, cur.y);
        case 'v' -> new Pos(cur.x, cur.y-1);
        case '<' -> new Pos(cur.x-1, cur.y);
        case '^' -> new Pos(cur.x, cur.y+1);
        default -> throw new IllegalStateException("Unexpected value: " + d);
      };
      visitedHouses.add(cur);
    }

    return String.valueOf(visitedHouses.size());
  }

  public String part2() {
    Set<Pos> visitedHouses = new HashSet<>();
    Pos santa = new Pos(0,0);
    Pos robot = new Pos(0,0);
    visitedHouses.add(santa);
    visitedHouses.add(robot);
    boolean santaToMove = true;
    for (char d : this.formattedInput) {
      Pos oldPos = santaToMove ? santa : robot;
      Pos newPos;
      if (d == '>') {
        newPos = new Pos(oldPos.x+1, oldPos.y);
      } else if (d == 'v') {
        newPos = new Pos(oldPos.x, oldPos.y-1);
      } else if (d == '<') {
        newPos = new Pos(oldPos.x-1, oldPos.y);
      } else {
        newPos = new Pos(oldPos.x, oldPos.y+1);
      }
      if (santaToMove) {
        santa = newPos;
      } else {
        robot = newPos;
      }
      visitedHouses.add(newPos);
      santaToMove = !santaToMove;
    }
    return String.valueOf(visitedHouses.size());
  }

  record Pos(int x, int y) {}
}