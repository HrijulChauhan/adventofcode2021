package com.yoki.advent_of_code.aoc.days2015;

import static java.lang.Math.abs;
import static java.util.stream.Collectors.joining;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.List;

public class Day11 extends AocDay {

  private final String ip;
  private final String possibleLetters;
  private final List<Character> excludedLetters;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day11(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.strip();
    this.possibleLetters = "abcdefghijklmnopqrstuvwxyz";
    this.excludedLetters = List.of('i','o','l');
  }

  public String part1() {
    var pass = StringUtil.stringToChars(this.ip);
    while (isInvalid(pass)) incrementPassword(pass);
    return pass.stream().map(String::valueOf).collect(joining());
  }


  public String part2() {
    var pass = StringUtil.stringToChars(part1());
    incrementPassword(pass);
    while (isInvalid(pass)) incrementPassword(pass);
    return pass.stream().map(String::valueOf).collect(joining());
  }

  private void incrementPassword(List<Character> pass) {
    for (int i = pass.size()-1; i > 0; i--) {
      Character c = pass.get(i);
      int j = this.possibleLetters.indexOf(c);
      if (j < this.possibleLetters.length()-1) {
        pass.set(i, this.possibleLetters.charAt(j+1));
        return;
      }
      pass.set(i, this.possibleLetters.charAt(0));
    }
  }

  private boolean isInvalid(List<Character> chars) {
    return !containsNoExcluded(chars) || !containsThreeFollowings(chars) || !containsTwoPairsNonOverlapping(chars);
  }

  private boolean containsNoExcluded(List<Character> chars) {
    return chars.stream().noneMatch(this.excludedLetters::contains);
  }

  private boolean containsThreeFollowings(List<Character> chars) {
    for (int i = 2; i < chars.size(); i++) {
      String triple = "" + chars.get(i - 2) + chars.get(i - 1) + chars.get(i);
      if (this.possibleLetters.contains(triple)) return true;
    }
    return false;
  }

  private boolean containsTwoPairsNonOverlapping(List<Character> chars) {
    int n = 0;
    char f = '!';
    for (int i = 1; i < chars.size(); i++) {
      if (chars.get(i-1).equals(chars.get(i)) && (f == '!' || f != chars.get(i))) {
        f = chars.get(i);
        n++;
        i++;
      }
    }
    return n > 1;
  }
}