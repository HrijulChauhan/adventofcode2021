package com.yoki.advent_of_code.aoc.days2015;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class Day6 extends AocDay {

  private final List<String> ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day6(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    boolean[][] grid = new boolean[1000][1000];
    fillGrid(grid);

    long c = 0L;
    for (int i = 0; i < 1000; i++) {
      for (int j = 0; j < 1000; j++) {
        c += grid[i][j] ? 1 : 0;
      }
    }
    return String.valueOf(c);
  }

  private void fillGrid(boolean[][] grid) {
    for (Action action : ip.stream().map(this::squareToToggle).toList()) {
      for (int x = action.from.x; x <= action.to.x; x++) {
        for (int y = action.from.y; y <= action.to.y; y++) {
          grid[x][y] = action.t ? !grid[x][y] : action.onOff;
        }
      }
    }
  }

  public String part2() {
    int[][] grid = new int[1000][1000];
    for (int i = 0; i < 1000; i++) {
      Arrays.fill(grid[i], 0);
    }
    fillGrid(grid);

    long c = 0L;
    for (int i = 0; i < 1000; i++) {
      for (int j = 0; j < 1000; j++) {
        c += grid[i][j];
      }
    }
    return String.valueOf(c);
  }

  private void fillGrid(int[][] grid) {
    for (Action action : ip.stream().map(this::squareToToggle).toList()) {
      for (int x = action.from.x; x <= action.to.x; x++) {
        for (int y = action.from.y; y <= action.to.y; y++) {
          actionOnGrid(grid, action, x, y);
        }
      }
    }
  }

  private void actionOnGrid(int[][] grid, Action action, int x, int y) {
    grid[x][y] += action.t ? 2 : action.onOff ? 1 : -1;
    if (grid[x][y] < 0) grid[x][y] = 0;
  }

  record Pos(int x, int y){}

  record Action(boolean t, boolean onOff, Pos from, Pos to){}

  public Action squareToToggle(String line) {
    String[] split = line.split(" through ");

    String[] xs = split[0].replace("turn ", "").split(" ")[1].split(",");
    Pos x = new Pos(Integer.parseInt(xs[0]), Integer.parseInt(xs[1]));

    String[] ys = split[1].split(",");
    Pos y = new Pos(Integer.parseInt(ys[0]), Integer.parseInt(ys[1]));

    if (line.startsWith("turn on")) {
      return new Action(false, true, x, y);
    }
    if (line.startsWith("turn off")) {
      return new Action(false, false, x, y);
    }
    return new Action(true, false, x, y);
  }

}