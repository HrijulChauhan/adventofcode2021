package com.yoki.advent_of_code.aoc.days2015;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.paukov.combinatorics3.Generator;

public class Day9 extends AocDay {

  private List<String> ip;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day9(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().toList();
  }

  public String part1() {
    // kruskal
    // Turns out we can't use kruskal because we need to visit each node exactly once
    //    List<Edge> res = new ArrayList<>();
    //    UnionFind uf = new UnionFind(names.size()-1);
    //    edges.sort(Comparator.comparingInt(a -> a.weight));
    //    for (var e : edges) {
    //      if (uf.find(e.source) != uf.find(e.dest)) {
    //        res.add(e);
    //        uf.union(e.source, e.dest, true);
    //      }
    //    }

    Graph graph = new Graph(this.ip);
    return String.valueOf(Generator.permutation(graph.names)
        .simple().stream()
        .mapToInt(p -> getTotalCost(p, graph, 99999))
        .min().orElse(0));
  }

  public String part2() {
    Graph graph = new Graph(this.ip);
    return String.valueOf(Generator.permutation(graph.names)
        .simple().stream()
        .mapToInt(p -> getTotalCost(p, graph, 0))
        .max().orElse(0));
  }

  private int getTotalCost(List<String> next, Graph graph, int def) {
    int res = 0;
    for (int i = 0; i < next.size() - 1; i++) {
      res += graph.getWeightOfSegment(next.get(i), next.get(i + 1)).orElse(def);
    }
    return res;
  }

  static class Graph {

    private final List<Edge<String>> edges;
    private final Set<String> names;

    public Graph(List<String> ip) {
      List<Edge<String>> edgeList = new ArrayList<>();
      this.names = new HashSet<>();
      for (String line : ip) {
        String[] split = line.split(" to ");
        String[] split2 = split[1].split(" = ");
        String node1 = split[0];
        String node2 = split2[0];
        int weight = Integer.parseInt(split2[1]);
        names.add(node1);
        names.add(node2);
        edgeList.add(new Edge<>(node1, node2, weight));
      }
      this.edges = edgeList;
    }

    public Optional<Integer> getWeightOfSegment(String source, String dest) {
      for (Edge<String> edge : edges) {
        if ((edge.dest.equals(dest) && edge.source.equals(source)) || (edge.dest.equals(source) && edge.source.equals(dest))) {
          return Optional.of(edge.weight);
        }
      }
      return Optional.empty();
    }
  }

  static record Edge<T>(T source, T dest, int weight) {}

}
