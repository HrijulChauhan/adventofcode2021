package com.yoki.advent_of_code.aoc.days2015;

import static com.yoki.advent_of_code.utils.CollectionUtil.asList;
import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import org.paukov.combinatorics3.Generator;

public class Day13 extends AocDay {


  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day13(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    Table table = new Table(this.input);
    return String.valueOf(table.maximalArrangement());
  }

  public String part2() {
    Table table = new Table(this.input + "Alecs w g 0 h u b s b t A.");
    return String.valueOf(table.maximalArrangement());
  }

  static class Table {

    private final List<Edge<String>> people;

    public Table(String ip) {
      this.people = ip.lines().map(this::toEdge).toList();
    }

    public int maximalArrangement() {
      List<String> names = getNames();
      String first = names.remove(0);

      return Generator.permutation(names)
          .simple()
          .stream()
          .mapToInt(p -> getTotalCost(asList(p, first)))
          .max().orElse(0);
    }

    public int getTotalCost(List<String> arrangement) {
      int res = 0;
      for (int i = 0; i < arrangement.size(); i++) {
        res += findTotalCost(arrangement.get(i), arrangement.get((i + 1) % arrangement.size())).orElse(0);
      }
      return res;
    }

    public Optional<Integer> findTotalCost(String source, String dest) {
      return findCost(source, dest).flatMap(i -> findCost(dest, source).map(j -> i + j));
    }

    public Optional<Integer> findCost(String source, String dest) {
      for (Edge<String> edge : people)
        if (edge.dest.equals(dest) && edge.source.equals(source))
          return Optional.of(edge.cost);
      return Optional.empty();
    }

    public List<String> getNames() {
      return this.people.stream().map(Edge::source).distinct().collect(toList());
    }

    private Edge<String> toEdge(String s) {
      String[] split = s.replace(".", "").split(" ");
      int value = Integer.parseInt(split[3]);
      if (split[2].equals("lose")) {
        value = Math.negateExact(value);
      }
      return new Edge<>(split[0], split[10], value);
    }
  }

  static record Edge<T>(T source, T dest, int cost) {}
}