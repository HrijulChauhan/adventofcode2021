package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class Day1 extends AocDay {

  private final int[] inputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day1(String input, PrintStream output) {
    super(input, output);
    this.inputs = this.input.lines().mapToInt(Integer::parseInt).toArray();
  }

  public String part1() {
    return String.valueOf(countBiggerInList(this.inputs));
  }

  public String part2() {
    return String.valueOf(countBiggerInList(sumInputsBy(this.inputs, 3)));
  }

  private int[] sumInputsBy(int[] inputs, int n) {
    return StreamEx.ofSubLists(boxedInputs(inputs), n).map(IntStreamEx::of).mapToInt(IntStreamEx::sum).toArray();
  }

  private List<Integer> boxedInputs(int[] inputs) {
    return IntStreamEx.of(inputs).boxed().toList();
  }

  private long countBiggerInList(int[] inputs) {
    return IntStreamEx.of(inputs).pairMap((i, in) -> in - i).greater(0).count();
  }

}
