package com.yoki.advent_of_code.aoc.days2021;

import static com.yoki.advent_of_code.utils.CollectionUtil.enumerate;
import static com.yoki.advent_of_code.utils.CollectionUtil.enumerateStream;
import static java.lang.Character.isLetter;
import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import com.yoki.advent_of_code.utils.CollectionUtil.EnumeratedItem;
import com.yoki.advent_of_code.utils.StringUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Set;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;

public class Day23 extends AocDay {

  public static final List<Character> TYPE = List.of('A', 'B', 'C', 'D');

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day23(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    //   #############
    //   #...........#
    //   ###D#A#C#C###
    //     #D#A#B#B#
    //     #########

    String init = "DDAACBCB|...........";
    Integer bestSolution = findBestSolution(new State(init));
    return String.valueOf(bestSolution);
  }

  public String part2() {
    //   #############
    //   #...........#
    //   ###D#A#C#C###
    //     #D#C#B#A#
    //     #D#B#A#C#
    //     #D#A#B#B#
    //     #########

    String init2 = "DDDDACBACBABCACB|...........";
    State initState = new State(init2);
    Integer bestSolution2 = findBestSolution(initState);
    return String.valueOf(bestSolution2);
  }

  private static Integer findBestSolution(State initState) {
    // Dijksta's !
    PriorityQueue<StateWithCost> toVisit = new PriorityQueue<>();
    toVisit.add(new StateWithCost(0, initState));
    Set<State> visited = new HashSet<>();
    Map<State, Integer> currentCosts = new HashMap<>();
    Map<State, State> previous = new HashMap<>();

    while (!toVisit.isEmpty()) {
      StateWithCost current = toVisit.poll();
      visited.add(current.state());

      for (StateWithCost next : current.state().possibleNextStates()) {
        if (!visited.contains(next.state())) {
          currentCosts.putIfAbsent(next.state(), Integer.MAX_VALUE);
          var newCost = current.cost + next.cost;
          if (newCost < currentCosts.get(next.state())) {
            currentCosts.put(next.state, newCost);
            previous.put(next.state, current.state);
            toVisit.add(new StateWithCost(newCost, next.state()));
          }
        }
      }
    }
    State solved = currentCosts.keySet().stream().filter(State::isSolved).findFirst().get();
    // Show history
    State cur = solved;
    State pre;
    while ((pre = previous.get(cur)) != null) {
      System.out.println(pre);
      cur = pre;
    }
    return currentCosts.get(solved);
  }

  static record Room(char type, int index, List<Character> content) {

    public Room(char type, int index, String content) {
      this(type, index, StringUtil.stringToChars(content));
    }

    public boolean hasOnlyValidAmphipods() {
      return content.stream().allMatch(i -> i.equals(type));
    }

    public boolean isEmptyOrHasAllValidAmphipods() {
      return content.stream().allMatch(i -> i.equals('.') || i.equals(type));
    }

    public boolean hasAmphipodsWithWrongType() {
      return !isEmptyOrHasAllValidAmphipods();
    }

    @Override
    public String toString() {
      return StreamEx.of(content).joining();
    }
  }

  static class State {

    private final Map<Character, Integer> multipliers = Map.of('A', 1, 'B', 10, 'C', 100, 'D', 1000);
    private final Map<Character, Room> rooms = new HashMap<>();
    private final List<Character> hallway;

    public State(String init) {
      String[] roomAndHall = init.split("\\|");
      String roomInit = roomAndHall[0];
      int rSize = roomInit.length() / TYPE.size();
      int i = 0;
      for (var e : enumerate(TYPE)) {
        this.rooms.put(e.item(), new Room(e.item(), e.index() * 2 + 2, roomInit.substring(i, i + rSize)));
        i += rSize;
      }
      this.hallway = StringUtil.stringToChars(roomAndHall[1]);
    }

    private boolean isSolved() {
      return rooms.values().stream().allMatch(Room::hasOnlyValidAmphipods);
    }

    public List<StateWithCost> possibleNextStates() {
      List<StateWithCost> res = new ArrayList<>();
      res.addAll(possibleHallwayMoves());
      res.addAll(possibleRoomMoves());
      return res;
    }

    private List<StateWithCost> possibleHallwayMoves() {
      List<StateWithCost> res = new ArrayList<>();
      for (var apod : amphipodsInHallwayThatCanMove()) {
        var room = rooms.get(apod.item());
        if (hallwayPathIsClear(apod.index(), room.index())) {
          var y = room.content.lastIndexOf('.') + 1;
          var cost = (Math.abs(apod.index() - room.index()) + y) * multipliers.get(apod.item());

          State next = new State(this.toString());
          next.blankToHall(apod.index());
          next.moveToRoom(apod.item(), y);

          res.add(new StateWithCost(cost, next));
        }
      }
      return res;
    }

    private List<StateWithCost> possibleRoomMoves() {
      List<StateWithCost> res = new ArrayList<>();
      for (var room : roomsWithWrongAmphipods()) {
        var toMove = enumerateStream(room.content()).filter(i -> !i.item().equals('.')).findFirst().get();
        for (int i : legalHallwayIndexes()) {
          if (hallwayPathIsClear(i, room.index())) {
            var y = toMove.index() + 1;
            var cost = (Math.abs(room.index - i) + y) * multipliers.get(toMove.item());

            State next = new State(this.toString());
            next.blankToRoom(room.type, y);
            next.moveToHall(i, toMove.item());

            res.add(new StateWithCost(cost, next));
          }
        }
      }
      return res;
    }

    private void blankToRoom(char r, int pos) {
      moveToRoom(r, pos, '.');
    }

    private void blankToHall(int i) {
      moveToHall(i, '.');
    }

    private void moveToRoom(char r, int pos) {
      moveToRoom(r, pos, r);
    }

    private void moveToRoom(char r, int pos, char nr) {
      rooms.get(r).content.set(pos - 1, nr);
    }

    private void moveToHall(int i, char toMove) {
      hallway.set(i, toMove);
    }

    private List<Integer> legalHallwayIndexes() {
      List<Integer> invalid = rooms.values().stream().map(Room::index).toList();
      return enumerateStream(hallway).filter(e -> e.item().equals('.') &&
          !invalid.contains(e.index())).map(EnumeratedItem::index).toList();
    }

    private List<Room> roomsWithWrongAmphipods() {
      return StreamEx.of(rooms.values()).filter(Room::hasAmphipodsWithWrongType).toList();
    }

    private List<EnumeratedItem<Character>> amphipodsInHallwayThatCanMove() {
      return enumerateStream(hallway).filter(e -> isLetter(e.item()) && rooms.get(e.item()).isEmptyOrHasAllValidAmphipods())
          .toList();
    }

    private boolean hallwayPathIsClear(int start, int end) {
      return hallway.subList(min(start, end) + 1, max(start, end)).stream().allMatch(c -> c.equals('.'));
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      return StringUtils.equals(this.toString(), o.toString());
    }

    @Override
    public int hashCode() {
      return Objects.hash(toString());
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      rooms.values().forEach(r -> sb.append(r.toString()));
      return sb.append('|').append(StreamEx.of(hallway).joining()).toString();
    }
  }

  static record StateWithCost(int cost, State state) implements Comparable<StateWithCost> {

    @Override
    public int compareTo(StateWithCost other) {
      return Integer.compare(this.cost, other.cost);
    }
  }
}
