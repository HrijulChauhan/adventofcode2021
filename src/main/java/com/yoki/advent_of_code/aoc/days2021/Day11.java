package com.yoki.advent_of_code.aoc.days2021;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.Deque;

public class Day11 extends AocDay {

  protected static final int[][] ROTATIONS = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

  private int size;
  private int[][] map;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day11(String input, PrintStream output) {
    super(input, output);
    var outputValues = this.input.lines()
        .map(l -> l.chars().mapToObj(c -> (char)c).map(Object::toString).map(Integer::parseInt)
            .collect(toList())).collect(toList());

    this.size = outputValues.size();
    this.map = new int[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        map[i][j] = outputValues.get(i).get(j);
      }
    }
  }

  public String part1() {
    return String.valueOf(flashes(100));
  }

  public String part2() {
    return String.valueOf(flashSimultaneity());
  }

  private int flashSimultaneity() {
    int step = 0;
    while (!allZeros()) {
      increment();
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          if (map[i][j] < 10 && map[i][j] > 0) continue;
          processQueue(i, j);
        }
      }
      step++;
    }
    return step;
  }

  private void processQueue(int i, int j) {
    Deque<int[]> queue = new ArrayDeque<>();
    queue.add(new int[]{i, j});
    while (!queue.isEmpty()) {
      int[] pos = queue.pop();
      int ii = pos[0];
      int jj = pos[1];

      if (map[ii][jj] == 0) continue;

      if (map[ii][jj] < 9) {
        map[ii][jj]++;
        continue;
      }

      map[ii][jj] = 0;

      addRotations(queue, ii, jj);
    }
  }

  private void addRotations(Deque<int[]> queue, int ii, int jj) {
    for (int[] dir : ROTATIONS) {
      int newI = ii + dir[0];
      int newJ = jj + dir[1];
      if (0 <= newI && newI < size && 0 <= newJ && newJ < size && map[newI][newJ] != 0)
        queue.add(new int[]{newI, newJ});
    }
  }

  private int flashes(int n) {
    int flashes = 0;
    for (int step = 0; step < n; step++) {
      increment();
      for (int i = 0; i < this.size; i++) {
        for (int j = 0; j < this.size; j++) {
          if (map[i][j] < 10 && map[i][j] > 0) continue;
          flashes = getFlashes(flashes, i, j);
        }
      }
    }
    return flashes;
  }

  private int getFlashes(int flashes, int i, int j) {
    Deque<int[]> stack = new ArrayDeque<>();
    stack.push(new int[]{i, j});
    while (!stack.isEmpty()) {
      int[] pos = stack.pop();
      int ii = pos[0];
      int jj = pos[1];

      if (map[ii][jj] == 0) continue;
      if (map[ii][jj] < 9) {
        map[ii][jj]++;
        continue;
      }

      map[ii][jj] = 0;
      flashes++;

      addRotation(stack, ii, jj);
    }
    return flashes;
  }

  private void addRotation(Deque<int[]> stack, int ii, int jj) {
    for (int[] dir : ROTATIONS) {
      int newI = ii + dir[0];
      int newJ = jj + dir[1];
      if (0 <= newI && newI < size && 0 <= newJ && newJ < size && map[newI][newJ] != 0)
        stack.push(new int[]{newI, newJ});
    }
  }

  private boolean allZeros() {
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        if (map[i][j] != 0) return false;
      }
    }
    return true;
  }

  private void increment() {
    for (int i = 0; i < size; i++)
      for (int j = 0; j < size; j++)
        map[i][j]++;
  }
}
