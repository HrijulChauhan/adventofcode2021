package com.yoki.advent_of_code.aoc.days2021;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;
import one.util.streamex.IntStreamEx;

public class Day9 extends AocDay {

  protected static final int[][] ROTATIONS = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

  private int rowSize;
  private int colSize;
  private int[][] map;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day9(String input, PrintStream output) {
    super(input, output);
    List<List<Character>> outputValues = this.input.lines().map(l -> IntStreamEx.ofChars(l).mapToObj(c -> (char)c).collect(toList()))
        .collect(toList());

    this.rowSize = outputValues.size();
    this.colSize = outputValues.get(0).size();

    this.map = new int[rowSize][colSize];
    for (int i = 0; i < rowSize; i++) {
      for (int j = 0; j < colSize; j++) {
        map[i][j] = Integer.parseInt(outputValues.get(i).get(j).toString());
      }
    }
  }

  public String part1() {
    return String.valueOf(sumRisk(map));
  }

  public String part2() {
    return String.valueOf(sumBasin(map));
  }

  private int sumBasin(int[][] map) {
    List<Integer> sizes = new ArrayList<>();
    for (int i = 0; i < rowSize; i++) {
      for (int j = 0; j < colSize; j++) {
        if (map[i][j] == 9) continue;
        sizes.add(processQueue(map, i, j));
      }
    }
    sizes.sort(Integer::compareTo);
    return sizes.get(sizes.size() - 1) * sizes.get(sizes.size() - 2) * sizes.get(sizes.size() - 3);
  }

  private int processQueue(int[][] map, int i, int j) {
    Deque<int[]> queue = new ArrayDeque<>();
    queue.add(new int[]{i, j});

    int size = 0;
    while (!queue.isEmpty()) {
      int[] pos = queue.remove();
      int ii = pos[0];
      int jj = pos[1];
      if (map[ii][jj] == 9) continue;

      map[ii][jj] = 9;
      size++;

      for (int[] dir : ROTATIONS) {
        int newI = ii + dir[0];
        int newJ = jj + dir[1];
        if (0 <= newI && newI < rowSize && 0 <= newJ && newJ < colSize && map[newI][newJ] != 9)
          queue.add(new int[]{newI, newJ});
      }
    }
    return size;
  }

  private int sumRisk(int[][] map) {
    int res = 0;
    for (int i = 0; i < rowSize; i++) {
      for (int j = 0; j < colSize; j++) {
        int cur = map[i][j];
        if (isLowPoint(rowSize, colSize, map, i, j, cur)) {
          res += cur + 1;
        }
      }
    }
    return res;
  }

  private boolean isLowPoint(int rowSize, int colSize, int[][] map, int i, int j, int cur) {
    for (int[] dir : ROTATIONS) {
      int newI = i + dir[0];
      int newJ = j + dir[1];
      if (0 <= newI && newI < rowSize && 0 <= newJ && newJ < colSize && map[newI][newJ] <= cur) {
        return false;
      }
    }
    return true;
  }
}
