package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Collectors;

public class Day20 extends AocDay {

  private final List<String> ip;
  private final ImageEnhancer ie;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day20(String input, PrintStream output) {
    super(input, output);
    this.ip = this.input.lines().collect(Collectors.toList());
    this.ie = new ImageEnhancer(ip);
  }

  public String part1() {
    ie.process(2);
    ie.render();
    return String.valueOf(ie.pixelsLit());
  }

  public String part2() {
    ie.process(50);
    ie.render();
    return String.valueOf(ie.pixelsLit());
  }

  static class ImageEnhancer {

    private final char[] mapTable;
    private char[][] image;

    public ImageEnhancer(List<String> ip) {
      this.mapTable = ip.remove(0).toCharArray();
      ip.remove(0);

      this.image = new char[ip.get(0).length()][ip.size()];
      for (int i = 0; i < ip.size(); i++) {
        image[i] = ip.get(i).toCharArray();
      }
    }

    public void process(int steps) {
      for (int i = 0; i < steps; i++) {
        this.process(mapTable[0] == '.' ? mapTable[0] : i%2==0 ? mapTable[511] : mapTable[0], 1);
      }
    }

    public void process(char def, int border) {
      char[][] newImage = new char[image.length + (border*2)][image[0].length + (border*2)];
      for (int i = 0; i < image.length + (border*2); i++) {
        for (int j = 0; j < image[0].length + (border*2); j++) {
          newImage[i][j] = map(i-border, j-border, def);
        }
      }
      image = newImage;
    }

    private char map(int i, int j, char d) {
      return mapTable[sectionToInt(getSection(i, j, d))];
    }

    private char[] getSection(int i, int j, char d) {
      return new char[]{
          safeGet(i - 1, j - 1, d), safeGet(i - 1, j, d), safeGet(i - 1, j + 1, d),
          safeGet(i, j - 1, d), safeGet(i, j, d), safeGet(i, j + 1, d),
          safeGet(i + 1, j - 1, d), safeGet(i + 1, j, d), safeGet(i + 1, j + 1, d)
      };
    }

    private char safeGet(int i, int j, char def) {
      try {
        return image[i][j];
      } catch (Exception ignore) {
        return def;
      }
    }

    private int sectionToInt(char[] section) {
      StringBuilder bin = new StringBuilder();
      for (char c : section) {
        bin.append(c == '#' ? "1" : "0");
      }
      return Integer.parseInt(bin.toString(), 2);
    }

    public int pixelsLit() {
      int count = 0;
      for (char[] chars : image) {
        for (int j = 0; j < image[0].length; j++) {
          if (chars[j] == '#') count++;
        }
      }
      return count;
    }

    public void render() {
      for (char[] chars : image) {
        System.out.println(chars);
      }
    }
  }
}
