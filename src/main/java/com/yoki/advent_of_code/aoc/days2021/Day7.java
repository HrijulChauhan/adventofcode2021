package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import one.util.streamex.IntStreamEx;

public class Day7 extends AocDay {

  private final int[] inputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day7(String input, PrintStream output) {
    super(input, output);
    this.inputs = Arrays.stream(this.input.strip().split(",")).mapToInt(Integer::parseInt).toArray();
  }

  public String part1() {
    var res = new CrabShip(inputs).cheapestHorizontal();
    return String.valueOf(res);
  }

  public String part2() {
    return part1();
  }

  record CrabShip(int[] crabsPos) {

    public int cheapestHorizontal() {
      return IntStreamEx.range(IntStreamEx.of(crabsPos).max().orElse(0))
          .minByInt(this::getCheapestByI).orElse(0);
    }

    private int getCheapestByI(int i) {
      return IntStreamEx.of(crabsPos)
          .map(j -> j - i)
          .map(Math::abs)
          .map(this::fuel)
          .sum();
    }

    private int fuel(int n) {
      return n * (n + 1) / 2;
    }
  }

}
