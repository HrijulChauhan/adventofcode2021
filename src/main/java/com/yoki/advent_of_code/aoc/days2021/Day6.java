package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Arrays;
import one.util.streamex.LongStreamEx;

public class Day6 extends AocDay {

  private final int[] inputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day6(String input, PrintStream output) {
    super(input, output);
    this.inputs = Arrays.stream(this.input.strip().split(",")).mapToInt(Integer::parseInt).toArray();
  }

  public String part1() {
    return String.valueOf(new FishTank(inputs).growthPrediction(80));
  }

  public String part2() {
    return String.valueOf(new FishTank(inputs).growthPrediction(256));
  }

  class FishTank {

    private long[] fish;

    public FishTank(int[] inputs) {
      this.fish = new long[9];
      for (int i : inputs) fish[i]++;
    }

    public long growthPrediction(int days) {
      for (int day = 0; day < days; day++) {
        resetZeroDays();
        decrementAll();
      }
      return getSum();
    }

    private void resetZeroDays() {
      fish[7] += fish[0];
    }

    private void decrementAll() {
      this.fish = rotate(fish);
    }

    private long getSum() {
      return LongStreamEx.of(fish).sum();
    }

    private long[] rotate(long[] l) {
      return LongStreamEx.of(l).boxed().headTail((h, t) -> t.append(h)).mapToLong(Long::longValue).toArray();
    }
  }

}
