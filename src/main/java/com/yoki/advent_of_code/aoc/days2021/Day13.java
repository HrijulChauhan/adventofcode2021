package com.yoki.advent_of_code.aoc.days2021;

import static java.util.function.Predicate.not;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Day13 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day13(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(0);
  }

  public String part2() {
    List<String> outputValues = this.input.lines().toList();
    List<int[]> points = extractPoints(outputValues);
    List<Pair<String, Integer>> folds = extractPairs(outputValues);

    compute(points, folds);

    Set<Pair<Integer, Integer>> pairs = points.stream().map(p -> Pair.of(p[0], p[1])).collect(Collectors.toSet());
    for (int j = 0; j < 6; j++) {
      for (int i = 0; i < 40; i++) {
        if (pairs.contains(Pair.of(i, j))) out.print("#");
        else out.print(".");
      }
      out.println();
    }
    return String.valueOf(0);
  }

  private void compute(List<int[]> points, List<Pair<String, Integer>> folds) {
    for (var f : folds) {
      for (int[] p : points) {
        if (f.getKey().equals("x") && p[0] > f.getValue())
          p[0] = f.getValue() * 2 - p[0];
        else if (f.getKey().equals("y") && p[1] > f.getValue())
          p[1] = f.getValue() * 2 - p[1];
      }
    }
  }

  private List<Pair<String, Integer>> extractPairs(List<String> outputValues) {
    return StreamEx.of(outputValues).dropWhile(not(String::isBlank))
        .skip(1).map(l -> l.split(" ")[2]).map(f -> f.split("="))
        .map(i -> Pair.of(i[0], Integer.parseInt(i[1]))).toList();
  }

  private List<int[]> extractPoints(List<String> outputValues) {
    return StreamEx.of(outputValues).takeWhile(not(String::isBlank))
        .map(l -> StreamEx.split(l, ",").mapToInt(Integer::parseInt).toArray())
        .toList();
  }

}
