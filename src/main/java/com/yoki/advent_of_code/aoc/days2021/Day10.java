package com.yoki.advent_of_code.aoc.days2021;

import static java.util.stream.Collectors.toList;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.stream.Collectors;
import one.util.streamex.StreamEx;

public class Day10 extends AocDay {

  public static final Map<Character, Character> CHUNKS = Map.of('(', ')', '[', ']', '{', '}', '<',
      '>');
  public static final Map<Character, Integer> POINTS = Map.of(')', 3, ']', 57, '}', 1197, '>',
      25137);
  public static final List<Character> COMPLETE_POINTS = List.of(')', ']', '}', '>');

  private final List<List<Character>> outputValues;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day10(String input, PrintStream output) {
    super(input, output);
    this.outputValues = StreamEx.of(this.input.lines().collect(toList()))
        .map(l -> l.chars().mapToObj(c -> (char)c).toList()).toList();
  }

  public String part1() {
    return String.valueOf(corruptedLines(outputValues));
  }

  public String part2() {
    List<Long> points = StreamEx.of(outputValues).map(this::incompleteLine)
        .nonNull().filter(i -> i != 0)
        .sorted().toList();
    return String.valueOf(points.get(points.size() / 2));
  }

  private long incompleteLine(List<Character> line) {
    Stack<Character> stack = isCorrupted(line).orElse(new Stack<>());
    Collections.reverse(stack);
    return stack.stream().map(COMPLETE_POINTS::indexOf).map(i -> i + 1)
        .mapToLong(l -> (long)l).reduce(0, (a, b) -> a * 5 + b);
  }

  private Optional<Stack<Character>> isCorrupted(List<Character> line) {
    Stack<Character> stack = new Stack<>();
    for (Character c : line) {
      if (CHUNKS.containsKey(c)) stack.add(CHUNKS.get(c));
      else if (CHUNKS.containsValue(c) && !stack.pop().equals(c))
        return Optional.empty();
    }
    return Optional.of(stack);
  }

  private int corruptedLines(List<List<Character>> outputValues) {
    int sum = 0;
    for (var line : outputValues) {
      Stack<Character> stack = new Stack<>();
      for (var c : line) {
        if (CHUNKS.containsKey(c)) stack.add(CHUNKS.get(c));
        else if (CHUNKS.containsValue(c)) {
          Character pop = stack.pop();
          if (!pop.equals(c)) {
            sum += POINTS.get(c);
            break;
          }
        }
      }
    }
    return sum;
  }
}
