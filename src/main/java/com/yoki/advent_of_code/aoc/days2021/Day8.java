package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class Day8 extends AocDay {

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day8(String input, PrintStream output) {
    super(input, output);
  }

  public String part1() {
    return String.valueOf(0);
  }

  public String part2() {
    List<String[]> outputValues = this.input.lines().map(l -> l.split(" \\| ")).collect(Collectors.toList());
    int sumres = outputValues.stream()
        .map(s -> Pair.of(s[0], s[1]))
        .mapToInt(this::getOutputForOneLine).sum();
    return String.valueOf(sumres);
  }

  private int getOutputForOneLine(Pair<String, String> output) {
    Map<Integer, Set<Character>> d = StreamEx.split(output.getLeft(), " ")
        .filter(s -> List.of(2, 4).contains(s.length()))
        .toMap(String::length, this::stringToCharSet);

    String res = StreamEx.split(output.getRight(), " ")
        .map(s -> mapLengthToSegment(s, d)).joining();

    return Integer.parseInt(res);
  }

  private String mapLengthToSegment(String o, Map<Integer, Set<Character>> d) {
    int l = o.length();

    if (l == 5) {
      Set<Character> s = stringToCharSet(o);
      if (getIntersections(s, d.get(2)) == 2) return "3";
      if (getIntersections(s, d.get(4)) == 2) return "2";
      return "5";
    }

    if (l == 6) {
      Set<Character> s = stringToCharSet(o);
      if (getIntersections(s, d.get(2)) == 1) return "6";
      if (getIntersections(s, d.get(4)) == 4) return "9";
      return "0";
    }

    return Map.of(2, "1", 3, "7", 4, "4", 7, "8").get(l);
  }

  private long getIntersections(Set<Character> s, Set<Character> d) {
    return s.stream().filter(d::contains).count();
  }

  private Set<Character> stringToCharSet(String o) {
    return IntStreamEx.ofChars(o).mapToObj(c -> (char) c).toSet();
  }
}
