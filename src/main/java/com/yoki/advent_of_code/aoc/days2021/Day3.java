package com.yoki.advent_of_code.aoc.days2021;

import com.yoki.advent_of_code.aoc.AocDay;
import java.io.PrintStream;
import java.util.List;
import one.util.streamex.IntStreamEx;
import one.util.streamex.StreamEx;

public class Day3 extends AocDay {

  private final List<String> inputs;

  /**
   * Prepare/parse the input in preparation for running the parts.
   *
   * @param input  the entire problem input as downloaded
   * @param output any display/debug output will be sent to output
   */
  public Day3(String input, PrintStream output) {
    super(input, output);
    this.inputs = this.input.lines().toList();
  }

  public String part1() {
    String gamma = IntStreamEx.range(inputs.get(0).length())
        .mapToObj(this::occurenceOfOne)
        .map(this::getMostCommon)
        .joining();

    int res = binToInt(gamma) * binToInt(reverseBinaryString(gamma));
    return String.valueOf(res);
  }

  public String part2() {
    return part1();
  }

  private int binToInt(String gamma) {
    return Integer.parseInt(gamma, 2);
  }

  private String reverseBinaryString(String bin) {
    return bin.replace('0', 'x')
        .replace('1', '0')
        .replace('x', '1');
  }

  private String getMostCommon(Long sum) {
    return sum > inputs.size() / 2 ? "1" : "0";
  }

  private long occurenceOfOne(int i) {
    return StreamEx.of(inputs).map(j -> j.charAt(i)).filterBy(c -> c, '1').count();
  }
}
