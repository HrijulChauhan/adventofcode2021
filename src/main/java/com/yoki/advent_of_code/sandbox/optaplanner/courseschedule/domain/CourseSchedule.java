package com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@Data
@NoArgsConstructor
@PlanningSolution
public class CourseSchedule {

  @ValueRangeProvider(id = "availableRooms")
  @ProblemFactCollectionProperty
  private List<Integer> rooms = new ArrayList<>();

  @ValueRangeProvider(id = "availablePeriods")
  @ProblemFactCollectionProperty
  private List<Integer> periods = new ArrayList<>();

  @PlanningEntityCollectionProperty
  private List<Lecture> lectures = new ArrayList<>();

  @PlanningScore
  private HardSoftScore score;

  public void printCourseSchedule() {
    lectures.stream()
        .map(c -> "Lecture in room " + c.getRoomNumber().toString()
            + " during Period" + c.getPeriod().toString())
        .forEach(System.out::println);
  }
}
