package com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@Data
@NoArgsConstructor
@PlanningEntity
public class Lesson {

  @PlanningId
  private Long id;

  private String subject;
  private String teacher;
  private String studentGroup;

  @PlanningVariable(valueRangeProviderRefs = "timeslotRange")
  private Timeslot timeslot;

  @PlanningVariable(valueRangeProviderRefs = "roomRange")
  private Room room;

  public Lesson(Long id, String subject, String teacher, String studentGroup) {
    this.id = id;
    this.subject = subject;
    this.teacher = teacher;
    this.studentGroup = studentGroup;
  }

  @Override
  public String toString() {
    return subject + "(" + id + ")";
  }
}
