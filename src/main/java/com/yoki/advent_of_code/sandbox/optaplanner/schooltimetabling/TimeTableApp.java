package com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling;

import static java.util.stream.Collectors.joining;

import com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain.Lesson;
import com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain.Room;
import com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain.TimeTable;
import com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain.Timeslot;
import com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.solver.TimeTableConstraintProvider;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.SolverConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TimeTableApp {
  private static final Logger LOGGER = LoggerFactory.getLogger(TimeTableApp.class);

  public static void main(String[] args) {
    SolverFactory<TimeTable> solverFactory = SolverFactory.create(new SolverConfig()
        .withSolutionClass(TimeTable.class)
        .withEntityClasses(Lesson.class)
        .withConstraintProviderClass(TimeTableConstraintProvider.class)
        // The solver runs only for 5 seconds on this small dataset.
        // It's recommended to run for at least 5 minutes ("5m") otherwise.
        .withTerminationSpentLimit(Duration.ofSeconds(10)));

    // Load the problem
    TimeTable problem = generateDemoData();

    // Solve the problem
    Solver<TimeTable> solver = solverFactory.buildSolver();
    TimeTable solution = solver.solve(problem);

    printTimetable(solution);
  }

  private static TimeTable generateDemoData() {
    List<Timeslot> timeslots = new ArrayList<>(10);
    timeslots.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
    timeslots.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
    timeslots.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
    timeslots.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
    timeslots.add(new Timeslot(DayOfWeek.MONDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));

    timeslots.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(8, 30), LocalTime.of(9, 30)));
    timeslots.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(9, 30), LocalTime.of(10, 30)));
    timeslots.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(10, 30), LocalTime.of(11, 30)));
    timeslots.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(13, 30), LocalTime.of(14, 30)));
    timeslots.add(new Timeslot(DayOfWeek.TUESDAY, LocalTime.of(14, 30), LocalTime.of(15, 30)));

    List<Room> rooms = new ArrayList<>(3);
    rooms.add(new Room("Room A"));
    rooms.add(new Room("Room B"));
    rooms.add(new Room("Room C"));

    List<Lesson> lessons = new ArrayList<>(20);
    long id = 0;
    lessons.add(new Lesson(id++, "Math", "A. Turing", "9th grade"));
    lessons.add(new Lesson(id++, "Math", "A. Turing", "9th grade"));
    lessons.add(new Lesson(id++, "Physics", "M. Curie", "9th grade"));
    lessons.add(new Lesson(id++, "Chemistry", "M. Curie", "9th grade"));
    lessons.add(new Lesson(id++, "Biology", "C. Darwin", "9th grade"));
    lessons.add(new Lesson(id++, "History", "I. Jones", "9th grade"));
    lessons.add(new Lesson(id++, "English", "I. Jones", "9th grade"));
    lessons.add(new Lesson(id++, "English", "I. Jones", "9th grade"));
    lessons.add(new Lesson(id++, "Spanish", "P. Cruz", "9th grade"));
    lessons.add(new Lesson(id++, "Spanish", "P. Cruz", "9th grade"));

    lessons.add(new Lesson(id++, "Math", "A. Turing", "10th grade"));
    lessons.add(new Lesson(id++, "Math", "A. Turing", "10th grade"));
    lessons.add(new Lesson(id++, "Math", "A. Turing", "10th grade"));
    lessons.add(new Lesson(id++, "Physics", "M. Curie", "10th grade"));
    lessons.add(new Lesson(id++, "Chemistry", "M. Curie", "10th grade"));
    lessons.add(new Lesson(id++, "French", "M. Curie", "10th grade"));
    lessons.add(new Lesson(id++, "Geography", "C. Darwin", "10th grade"));
    lessons.add(new Lesson(id++, "History", "I. Jones", "10th grade"));
    lessons.add(new Lesson(id++, "English", "P. Cruz", "10th grade"));
    lessons.add(new Lesson(id++, "Spanish", "P. Cruz", "10th grade"));

    return new TimeTable(timeslots, rooms, lessons);
  }

  private static void printTimetable(TimeTable timeTable) {
    LOGGER.info("");
    List<Room> roomList = timeTable.getRooms();
    List<Lesson> lessonList = timeTable.getLessons();
    Map<Timeslot, Map<Room, List<Lesson>>> lessonMap = lessonList.stream()
        .filter(lesson -> lesson.getTimeslot() != null && lesson.getRoom() != null)
        .collect(Collectors.groupingBy(Lesson::getTimeslot, Collectors.groupingBy(Lesson::getRoom)));
    LOGGER.info("|            | " + roomList.stream().map(room -> String.format("%-10s", room.getName()))
        .collect(joining(" | ")) + " |");
    LOGGER.info("|" + "------------|".repeat(roomList.size() + 1));
    for (Timeslot timeslot : timeTable.getTimeslots()) {
      List<List<Lesson>> cellList = roomList.stream()
          .map(room -> {
            Map<Room, List<Lesson>> byRoomMap = lessonMap.get(timeslot);
            if (byRoomMap == null)
              return Collections.<Lesson>emptyList();

            List<Lesson> cellLessonList = byRoomMap.get(room);
            return Objects.requireNonNullElse(cellLessonList, Collections.<Lesson>emptyList());
          })
          .collect(Collectors.toList());

      LOGGER.info("| " + String.format("%-10s",
          timeslot.getDayOfWeek().toString().substring(0, 3) + " " + timeslot.getStartTime()) + " | "
          + cellList.stream().map(cellLessonList -> String.format("%-10s",
              cellLessonList.stream().map(Lesson::getSubject).collect(joining(", "))))
          .collect(joining(" | "))
          + " |");
      LOGGER.info("|            | "
          + cellList.stream().map(cellLessonList -> String.format("%-10s",
              cellLessonList.stream().map(Lesson::getTeacher).collect(joining(", "))))
          .collect(joining(" | "))
          + " |");
      LOGGER.info("|            | "
          + cellList.stream().map(cellLessonList -> String.format("%-10s",
              cellLessonList.stream().map(Lesson::getStudentGroup).collect(joining(", "))))
          .collect(joining(" | "))
          + " |");
      LOGGER.info("|" + "------------|".repeat(roomList.size() + 1));
    }
    List<Lesson> unassignedLessons = lessonList.stream()
        .filter(lesson -> lesson.getTimeslot() == null || lesson.getRoom() == null)
        .collect(Collectors.toList());
    if (!unassignedLessons.isEmpty()) {
      LOGGER.info("");
      LOGGER.info("Unassigned lessons");
      for (Lesson lesson : unassignedLessons) {
        LOGGER.info("  " + lesson.getSubject() + " - " + lesson.getTeacher() + " - " + lesson.getStudentGroup());
      }
    }
  }
}
