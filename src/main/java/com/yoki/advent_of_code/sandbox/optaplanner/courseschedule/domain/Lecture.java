package com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@Setter
@NoArgsConstructor
@PlanningEntity
public class Lecture {

  @PlanningId
  private Long id;
  private Integer roomNumber;
  private Integer period;

  public Lecture(Long id) {
    this.id = id;
  }

  @PlanningVariable(valueRangeProviderRefs = {"availableRooms"})
  public Integer getRoomNumber() {
    return roomNumber;
  }

  @PlanningVariable(valueRangeProviderRefs = {"availablePeriods"})
  public Integer getPeriod() {
    return period;
  }
}
