package com.yoki.advent_of_code.sandbox.optaplanner.schooltimetabling.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Room {

  private String name;

  @Override
  public String toString() {
    return name;
  }
}
