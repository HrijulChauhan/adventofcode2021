package com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.constraint;

import static org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore.ONE_HARD;
import static org.optaplanner.core.api.score.stream.Joiners.equal;

import com.yoki.advent_of_code.sandbox.optaplanner.courseschedule.domain.Lecture;
import org.optaplanner.core.api.score.stream.Constraint;
import org.optaplanner.core.api.score.stream.ConstraintFactory;
import org.optaplanner.core.api.score.stream.ConstraintProvider;

public class CourseScheduleConstraintProvider implements ConstraintProvider {

  @Override
  public Constraint[] defineConstraints(ConstraintFactory constraintFactory) {
    return new Constraint[]{
        roomConflict(constraintFactory)
    };
  }

  public Constraint roomConflict(ConstraintFactory factory) {
    return factory
        .forEachUniquePair(Lecture.class, equal(Lecture::getRoomNumber), equal(Lecture::getPeriod))
        .penalize(ONE_HARD)
        .asConstraint("Room conflict");
  }
}
