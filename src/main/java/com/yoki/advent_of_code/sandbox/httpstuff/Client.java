package com.yoki.advent_of_code.sandbox.httpstuff;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.concurrent.CompletableFuture;
import lombok.SneakyThrows;

public class Client {

  @SneakyThrows
  public static void main(String[] args) {
    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder(URI.create("http://localhost:8003/hello")).GET().build();
    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
    System.out.println(response.statusCode());
    System.out.println(response.body());

    CompletableFuture<HttpResponse<String>> future = client.sendAsync(request, BodyHandlers.ofString());
    System.out.println(future.get().body());
  }
}
