package com.yoki.advent_of_code.sandbox.httpstuff;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import lombok.SneakyThrows;

public class Server {

  @SneakyThrows
  public static void main(String[] args) {
    HttpHandler handler = he -> {
      String body = "bob";
      he.sendResponseHeaders(200, body.length());
      try (OutputStream os = he.getResponseBody()) {
        os.write(body.getBytes());
      }
    };

    var hs = HttpServer.create(new InetSocketAddress(8003), 0);
    hs.createContext("/hello", handler);
    hs.start();
  }
}
