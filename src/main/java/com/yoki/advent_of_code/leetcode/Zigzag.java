package com.yoki.advent_of_code.leetcode;

import java.util.Arrays;

public class Zigzag {

  public static void main(String[] args) {
    String input = "PAYPALISHIRING";
    String res = convert(input, 3);
    System.out.println(res);
  }

  static String convert(String s, int numRows) {
    String[] res = new String[numRows];
    Arrays.fill(res, "");

    int pos = 0;
    int climbing = -1;
    for (int i = 0; i < s.length(); i++, pos += climbing) {
      if (pos == numRows - 1 || pos == 0) climbing = -climbing;
      res[pos] += s.charAt(i);
    }

    return String.join("", res);
  }
}
