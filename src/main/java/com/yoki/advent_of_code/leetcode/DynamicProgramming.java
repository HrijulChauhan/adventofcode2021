package com.yoki.advent_of_code.leetcode;

import static com.yoki.advent_of_code.utils.CollectionUtil.asSet;
import static org.apache.commons.lang3.math.NumberUtils.min;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import one.util.streamex.IntStreamEx;
import org.apache.commons.lang3.tuple.Pair;

public class DynamicProgramming {

  public static void main(String[] args) {
//    System.out.println(coinChange(new int[]{1, 3, 4, 5}, 7));
//    System.out.println(partitionEqualSubsetSum(List.of(2, 2, 3, 5)));
    System.out.println(longestPalindromicSubstring("babad"));
    System.out.println("---");
    System.out.println(longestPalindromicSubstring("sbbd"));
  }

  /**
   * Using dynamic programming
   **/
  static int coinChange(int[] coins, int amount) {
    int max = amount + 1;
    int[] dp = new int[max];
    Arrays.fill(dp, max);
    dp[0] = 0;

    for (int i = 1; i < max; i++) {
      for (int c : coins) {
        if (i - c >= 0)
          dp[i] = min(dp[i], 1 + dp[i - c]);
      }
    }

    return dp[amount] != max ? dp[amount] : -1;
  }

  static boolean partitionEqualSubsetSum(List<Integer> nums) {
    int sum = IntStreamEx.of(nums).sum();
    if (sum % 2 == 1)
      return false;

    int target = sum / 2;

    Set<Integer> dp = asSet(0);
    for (int i : nums) {
      Set<Integer> nextDp = new HashSet<>(dp);
      for (var t : dp) {
        int n = t + i;
        if (n == target)
          return true;
        nextDp.add(n);
      }
      dp = nextDp;
    }

    return dp.contains(target);
  }

  static String longestPalindromicSubstring(String input) {
    Pair<Integer, Integer> pos = Pair.of(0,0);
    for (int i = 0; i < input.length(); i++) {
      pos = longestPalindrome(input, pos, i, i);
      pos = longestPalindrome(input, pos, i, i+1);
    }
    return input.substring(pos.getLeft(), pos.getRight() + 1);
  }

  private static Pair<Integer, Integer> longestPalindrome(String input, Pair<Integer, Integer> pos, int l, int r) {
    Pair<Integer, Integer> max = pos;
    while (l >= 0 && r < input.length() && input.charAt(l) == input.charAt(r)) {
      if (subSize(l, r) > subSize(pos))
        max = Pair.of(l,r);
      l--;
      r++;
    }
    return max;
  }

  private static int subSize(Pair<Integer, Integer> pos) {
    return subSize(pos.getLeft(), pos.getRight());
  }

  private static int subSize(int l, int r) {
    return r - l + 1;
  }
}
